package com.bajaj.classes;

import java.sql.Date;
import java.util.Arrays;

public class Request {
	private String fullName;
	private Date dob;
	private String emailId;
	private String collegeRollNumber;
	private char [] charArray;
	private int [] intArray;
	public Request() {
		super();
	}
	public Request(String fullName, Date dob, String emailId, String collegeRollNumber, char[] charArray,
			int[] intArray) {
		super();
		this.fullName = fullName;
		this.dob = dob;
		this.emailId = emailId;
		this.collegeRollNumber = collegeRollNumber;
		this.charArray = charArray;
		this.intArray = intArray;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCollegeRollNumber() {
		return collegeRollNumber;
	}
	public void setCollegeRollNumber(String collegeRollNumber) {
		this.collegeRollNumber = collegeRollNumber;
	}
	public char[] getCharArray() {
		return charArray;
	}
	public void setCharArray(char[] charArray) {
		this.charArray = charArray;
	}
	public int[] getIntArray() {
		return intArray;
	}
	public void setIntArray(int[] intArray) {
		this.intArray = intArray;
	}
	@Override
	public String toString() {
		return "Request [fullName=" + fullName + ", dob=" + dob + ", emailId=" + emailId + ", collegeRollNumber="
				+ collegeRollNumber + ", charArray=" + Arrays.toString(charArray) + ", intArray="
				+ Arrays.toString(intArray) + "]";
	}
	
}
