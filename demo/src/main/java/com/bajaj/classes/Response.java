package com.bajaj.classes;

import java.util.Arrays;

public class Response {
	private String status;
	private String userId;
	private String emailId;
	private String collegeRollNumber;
	private char [] charArray;
	private int [] intArray;
	public Response() {
		super();
	}
	public Response(String status, String userId, String emailId, String collegeRollNumber, char[] charArray,
			int[] intArray) {
		super();
		this.status = status;
		this.userId = userId;
		this.emailId = emailId;
		this.collegeRollNumber = collegeRollNumber;
		this.charArray = charArray;
		this.intArray = intArray;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCollegeRollNumber() {
		return collegeRollNumber;
	}
	public void setCollegeRollNumber(String collegeRollNumber) {
		this.collegeRollNumber = collegeRollNumber;
	}
	public char[] getCharArray() {
		return charArray;
	}
	public void setCharArray(char[] charArray) {
		this.charArray = charArray;
	}
	public int[] getIntArray() {
		return intArray;
	}
	public void setIntArray(int[] intArray) {
		this.intArray = intArray;
	}
	@Override
	public String toString() {
		return "Response [status=" + status + ", userId=" + userId + ", emailId=" + emailId + ", collegeRollNumber="
				+ collegeRollNumber + ", charArray=" + Arrays.toString(charArray) + ", intArray="
				+ Arrays.toString(intArray) + "]";
	}
	
	
}
