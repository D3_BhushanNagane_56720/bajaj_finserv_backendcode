package com.bajaj.classes;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	
	@PostMapping("/bfhl")
	public Response postApi(@RequestBody Request array ) {
		Response response = new Response();
		response.setStatus("true");
		response.setUserId("" +array.getFullName()+"_"+array.getDob());
		response.setCollegeRollNumber(array.getCollegeRollNumber());
		response.setCharArray(array.getCharArray());
		response.setIntArray(array.getIntArray());
		return response;
	}
}
